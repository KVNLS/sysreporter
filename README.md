# System Reporter

How to run ?

Download zip and run following commands:

```
npm install
npm start
```

Sample of metrics to provide:

```
{"metrics":[
  {
    "files": 576,
    "inodes": 7422,
    "recv": 0,
    "send": 0,
    "used": 261951488,
    "buff": 29339648,
    "cach": 84221952,
    "free": 16367108096,
    "usr": 0.109,
    "sys": 0.494,
    "idl": 99.358,
    "wai": 0.038,
    "hiq": 0,
    "siq": 0.002,
    "time": "16-03 10:08:28",
    "read": 40697.201,
    "writ": 19128.464,
    "1m": 0.02,
    "5m": 0.04,
    "15m": 0.05
  }
]}
```