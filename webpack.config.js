var HtmlWebpackPlugin = require('html-webpack-plugin');
/*Used to fix refresh with react-router as we do not use any server*/

module.exports = {
    entry: [
        './app/index.js'
    ],
    node: {
        __dirname: false,
        __filename: false
    },
    output: {
        path: __dirname,
        publicPath: '/',
        filename: 'bundle.js'
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
            test: /\.scss$/,
            use: [
              {
                loader: "style-loader"
              },
              {
                loader: "css-loader"
              },
              {
                loader: "sass-loader"
              }
            ]
          }
      ]
    },
    devServer: {
        historyApiFallback: true,
    },
    devtool: 'source-map',
    plugins: [
        new HtmlWebpackPlugin({
          template: 'index.html'
        })
    ]
};
