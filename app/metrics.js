import {parseDate} from './utils';

const label = {
    'writ': 'Write',
    'read': 'Read',
    'files': 'Files',
    'inodes': 'Inodes',
    'usr': 'User',
    'sys': 'System',
    'idl': 'Idl',
    'wai': 'Waiting',
    'hiq': 'Hiq',
    'siq': 'Siq',
    'used': 'Used',
    'buff': 'Buffers',
    'cach': 'Cached',
    'free': 'Free',
    'recv': 'Received',
    'send': 'Send',
    '1m': 'Last minute',
    '5m': 'Last 5 minutes',
    '15m': 'Last 15 minutes'
}
class Metrics {
  
    constructor(){
      this.data = {};
    }

    getLabel(key) {
        return label.hasOwnProperty(key) && label[key];
    }

    getMetrics () {
        const that = this;
        return new Promise((resolve,reject) => {
            if(that.data && that.data.metrics) {
                resolve(that.data.metrics);
            }else{
                fetch("../metrics.json")
                .then(response => response.json())
                .then(responseParsed => {
                    that.data = that._cleanMetrics(responseParsed.metrics);
                    resolve(that.data)
                })
                .catch(error => reject(error));
            }
        });
    }

    _cleanMetrics (metrics) {
        let newMetrics = [];
        metrics.forEach((metric) => {
            if(metric.time) {
                metric.time = parseDate(metric.time);
                metric.idl = metric.idl / 100;
                newMetrics.push(metric);
            }//else we don't have a time, we can't use this data, so we don't push it our metrics.
        });
        //just in case metrics are not sorted by date
        newMetrics = newMetrics.sort(function(metric1, metric2) {
            return metric1.time - metric2.time;
        });
        return newMetrics;
    }
  
  }

const instance = new Metrics();

export default instance;