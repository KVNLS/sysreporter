import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import Metrics from '../metrics';
import {theme} from '../highchartsTheme';

class Chart extends React.Component {

    constructor(props) {
        super(props);
        
        const that = this;
        this._isMounted = false;
        this.state = {
            maxMode: false,
            maxMode: false,
            chartOptions: {
                chart: {
                    zoomType: 'x',
                    events: {
                        selection: that.updateLegend.bind(that),
                        load: function (){
                            that.chart = this;
                        }
                    }
                },
                rangeSelector: {
                    allButtonsEnabled: true
                },
                xAxis: {
                    type: 'datetime',
                    events: {
                        afterSetExtremes: that.updateLegend.bind(that)
                      }
                },
                yAxis: {
                    labels: {
                        formatter: function() {
                            return that.props.chartOptions.format(this.value);
                        }
                    }
                },
                tooltip: {
                    formatter: function() {
                        return '<b>' + Highcharts.dateFormat('%A, %b %e, %Y %H:%M:%S', this.x) 
                        + '<br/><span style="color:' + this.color + '">' 
                        + this.series.name + '</span>: <b>' 
                        + that.props.chartOptions.format(this.y) 
                        +'</b><br/>';
                    }
                },
                title: {
                    text: that.props.chartOptions.title
                }
              }
        };
        Highcharts.setOptions(theme);
    }

    componentDidUpdate() {
        this.chart && this.chart.xAxis.forEach( axis => {
            axis.setExtremes(this.props.startDate, this.props.endDate);
        });
    }

    componentDidMount() {
        const that = this;
        this._isMounted = true;
        Metrics.getMetrics().then(res => {
            //we don't want to perform this action if component is unmounted
            if(this._isMounted) {
                let series = [];
                that.props.chartOptions.data && that.props.chartOptions.data.forEach(key => {
                    series.push({
                        name: Metrics.getLabel(key),
                        data: res.filter( metric => metric.hasOwnProperty(key) && (metric[key] || metric[key] === 0))
                        .map(metric => [metric.time,metric[key]])
                    })
                });
                that.setState({ 
                    chartOptions : {
                        series: series
                    }
                });
            }//else do nothing
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    //function to update legend with max && min && average for each serie
    updateLegend(event) {
        const that = this,
            chart = that.chart ? that.chart : event.target && event.target.chart;
        chart && chart.update({
            legend: {
              labelFormatter: function() { 
                const defaultLabel ={
                    enabled: true,
                    borderRadius: 3,
                    borderColor: '#CC2929',
                    borderWidth: 1,
                    y: -23
                };
                let average = 0,
                    pointDisplayed = 0;
                this.points.forEach(point => {
                    let marker = ((point.y === this.dataMax && that.state.maxMode) 
                    || (point.y === this.dataMin && that.state.minMode)) ? {
                        radius: 3,
                        lineColor: '#CC2929',
                        lineWidth: 2,
                        fillColor: '#CC2929',
                        enabled: true
                    } : null,
                    dataLabel = (point.y === this.dataMax && that.state.maxMode) ? Object.assign({
                        formatter: function() {
                          return "Max : " + that.props.chartOptions.format(point.y);
                          }
                    }, defaultLabel) : (point.y === this.dataMin && that.state.minMode ? Object.assign({
                        formatter: function() {
                          return "Min : " + that.props.chartOptions.format(point.y);
                          }
                    }, defaultLabel): null);

                    average += point.isInside ? point.y : 0;
                    pointDisplayed += point.isInside ? 1 : 0;

                    point.applyOptions({
                        x: point.x,
                        y: point.y,
                        marker: marker,
                        dataLabels: dataLabel
                    });
                });
                average /= pointDisplayed;
                return this.name + '<br>'
                + '<span>Min: ' + that.props.chartOptions.format(this.dataMin) + '</span><br/>'
                + '<span>Max: ' + that.props.chartOptions.format(this.dataMax) + '</span><br/>'
                + '<span>Average: ' + that.props.chartOptions.format(average) + '</span><br/>';
              }
            }
        });
    }

    displayMax () {
        const that = this;
        this.setState({
            'maxMode': !this.state.maxMode
        }, () => {
            that.updateLegend();
        });
    }

    displayMin () {
        const that = this;
        this.setState({
            'minMode': !this.state.minMode
        }, () => {
            that.updateLegend();
        });
    }

    render() {
        return <div className="container">
        <button type="button" className ="button-component" onClick={this.displayMax.bind(this)}>{this.state.maxMode ? 'Hide Max': 'Show Max'}</button>
        <button type="button" className ="button-component" onClick={this.displayMin.bind(this)}>{this.state.minMode ? 'Hide Min': 'Show Min'}</button>
        <span className="help-message">Click and drag to zoom</span>
        <HighchartsReact
            allowChartUpdate={true}
            highcharts={Highcharts}
            options={this.state.chartOptions}
        />
        </div>;
    }
}

export default Chart;
