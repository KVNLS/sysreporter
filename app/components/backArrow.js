import React from 'react';
import {Link} from "react-router-dom";

class BackArrow extends React.Component {
    render() {
        return <div className="back-arrow-component">
            <Link className="border-effect" to="/">Home</Link>
        </div>
    }
}

export default BackArrow;
