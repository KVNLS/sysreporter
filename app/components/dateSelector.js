import React from 'react';
import Metrics from '../metrics';
import {getYYMMDD, getHM} from '../utils';

class DateSelector extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            minDate: null,
            maxDate: null
        };

        this.startDateInput = React.createRef();
        this.startTimeInput = React.createRef();
        this.endDateInput = React.createRef();
        this.endTimeInput = React.createRef();
        this.applyButton = React.createRef();
    }

    componentDidMount() {
        this._isMounted = true;
        this.resetExtremes();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    updateExtremes () {
        const dates = this.getParsedDates();

        dates.length === 2 && this.props.handler({
            startDate: dates[0],
            endDate: dates[1]
        });
    }

    getParsedDates() {
        const start = this.startDateInput.current.value + 'T' + this.startTimeInput.current.value + ':00.000+00:00',
            end = this.endDateInput.current.value + 'T' + this.endTimeInput.current.value + ':00.000+00:00';
        return start && end && [Date.parse(new Date(start)), Date.parse(new Date(end))];
    }

    updateInput () {
        const dates = this.getParsedDates();
        if(dates.length === 2){
            this.applyButton.current.disabled = dates[0] < dates[1] ? false : true;
        }//else do nothing error comes from getParsedDates
    }

    resetExtremes () {
        const that = this;
        Metrics.getMetrics().then( metrics => {
            if(that._isMounted) {
                let times = metrics.map(metric => {
                    return metric.time;
                }),
                maxTime = Math.max(...times),
                minTime = Math.min(...times);

                minTime && maxTime && that.props.handler({
                    startDate: minTime,
                    endDate: maxTime
                });
                minTime && maxTime && this.setState({
                    minDate: minTime,
                    maxDate: maxTime
                });
                this.startDateInput.current.value = getYYMMDD(minTime);
                this.startTimeInput.current.value = getHM(minTime);
                this.endDateInput.current.value = getYYMMDD(maxTime);
                this.endTimeInput.current.value = getHM(maxTime);
            }
        });
    }

    render() {
        return <div className="date-selector-component">
            <label htmlFor="start-date">From :</label>
            <input type="date" id="start-date" 
                ref={this.startDateInput}
                min={getYYMMDD(this.state.minDate)} 
                max={getYYMMDD(this.props.dates.endDate)}
                onChange={this.updateInput.bind(this)}
            ></input>
            <input type="time" id="start-time"
                ref={this.startTimeInput}
                min={getHM(this.state.minDate)} 
                max={getHM(this.props.dates.endDate)}
                onChange={this.updateInput.bind(this)}
            ></input>
            <label htmlFor="end-date">to :</label>
            <input type="date" id="end-date" 
                ref={this.endDateInput}
                min={getYYMMDD(this.props.dates.startDate)} 
                max={getYYMMDD(this.state.maxDate)}
                onChange={this.updateInput.bind(this)}
            ></input>
            <input type="time" id="start-time"
                ref={this.endTimeInput}
                min={getHM(this.props.dates.startDate)} 
                max={getHM(this.state.maxDate)}
                onChange={this.updateInput.bind(this)}
            ></input>
            <button type="button" 
                ref={this.applyButton}
                className ="button-component" 
                onClick={this.updateExtremes.bind(this)}
            >{'Apply'}</button>
            <button type="button" 
                className ="button-component" 
                onClick={this.resetExtremes.bind(this)}
            >{'Reset'}</button>
        </div>;
    }
}

export default DateSelector;
