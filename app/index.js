import React from 'react';
import {render} from 'react-dom';
import AppRouter from './appRouter';

require('../styles/main.scss');

const app = render(
    <AppRouter />,
    document.getElementById('root')
);
