const currentDate = new Date();

export function parseDate(date) {
    let parsedDate = date.split(' ');
    parsedDate = parsedDate.shift().split('-').reverse().join('-') + 'T' + parsedDate;
    return Date.parse(currentDate.getUTCFullYear() + '-' + parsedDate + '.000+00:00');
}

export function getYYMMDD(date) {
    const newDate = new Date(date),
        month = newDate.getUTCMonth() + 1;
    return newDate.getUTCFullYear() + '-' 
    + ( month < 10 ? '0' + month : month)  
    + '-' + newDate.getUTCDate();
}

export function getHM(date) {
    const newDate = new Date(date),
        hour = newDate.getUTCHours(),
        minutes = newDate.getUTCMinutes();
    return  ( hour < 10 ? '0' + hour : hour) + ':'
    + ( minutes < 10 ? '0' + minutes : minutes);
}
