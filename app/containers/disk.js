import React from 'react';
import Chart from '../components/chart';

const chartOptions = {
    format: function(value) {
        return parseInt(value);
    },
    title: 'File System',
    data: ['files', 'inodes']
},
chartOptions2 = {
    format: function(value) {
        return parseFloat(value / 1000).toFixed(2) + ' KB';
    },
    title: 'Disk Activity',
    data: ['read', 'writ']
};

class Disk extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="container">
        <Chart 
            chartOptions={chartOptions} 
            startDate={this.props.startDate} 
            endDate={this.props.endDate}
        />
        <Chart 
            chartOptions={chartOptions2} 
            startDate={this.props.startDate} 
            endDate={this.props.endDate}
        />
        </div>;
    }
}

export default Disk;
