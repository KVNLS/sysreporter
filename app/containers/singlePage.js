import React from 'react';
import CPU from './cpu';
import Network from './network';
import Memory from './memory';
import LoadAverage from './loadAverage';
import Disk from './disk';
import DateSelector from '../components/dateSelector';

class SinglePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            endDate: null
        };
    }

    updateDates (values) {
        values && this.setState({
            startDate: values.startDate ? values.startDate: this.state.startDate,
            endDate: values.endDate ? values.endDate: this.state.endDate
        });
    }

    render() {
        let chart;
        
        switch(this.props.location.pathname) {
            case '/network': 
                chart = <Network startDate={this.state.startDate} endDate={this.state.endDate}/>;
                break;
            case '/cpu':
                chart = <CPU startDate={this.state.startDate} endDate={this.state.endDate}/>;
                break;  
            case '/memory':
                chart = <Memory startDate={this.state.startDate} endDate={this.state.endDate}/>;
                break;    
            case '/disk':
                chart = <Disk startDate={this.state.startDate} endDate={this.state.endDate}/>;
                break;    
            case '/loadaverage':
                chart = <LoadAverage startDate={this.state.startDate} endDate={this.state.endDate}/>;
                break;    
            case '/dashboard':
            default:
                chart =(<div className='dashboard'>
                    <Network startDate={this.state.startDate} endDate={this.state.endDate}/> 
                    <CPU startDate={this.state.startDate} endDate={this.state.endDate}/>
                    <Memory startDate={this.state.startDate} endDate={this.state.endDate}/>
                    <Disk startDate={this.state.startDate} endDate={this.state.endDate}/>
                    <LoadAverage startDate={this.state.startDate} endDate={this.state.endDate}/>
                </div>);
                break;        
        }
        return <div className="single-page-container">
            <DateSelector handler={this.updateDates.bind(this)} dates={this.state} />
            {chart}
        </div>;
    }
}

export default SinglePage;
