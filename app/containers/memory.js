import React from 'react';
import Chart from '../components/chart';

const chartOptions = {
    format: function(value) {
        return parseFloat(value / 1000000).toFixed(2) + ' MB';
    },
    title: 'Memory',
    data: ['used', 'buff', 'cach', 'free']
};

class Memory extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="container">
        <Chart 
            chartOptions={chartOptions} 
            startDate={this.props.startDate} 
            endDate={this.props.endDate}
        />
        </div>;
    }
}

export default Memory;
