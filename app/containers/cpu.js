import React from 'react';
import Chart from '../components/chart';

const chartOptions = {
    format: function(value) {
        return parseFloat(value * 100).toFixed(2) +"%";
    },
    title: 'CPU Usage',
    data: ['usr', 'sys', 'idl', 'wai', 'hiq', 'siq']
};

class CPU extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <div className="container">
        <Chart 
            chartOptions={chartOptions} 
            startDate={this.props.startDate} 
            endDate={this.props.endDate}
        />
        </div>;
    }
}

export default CPU;
