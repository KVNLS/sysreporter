import React from 'react';
import Chart from '../components/chart';

const chartOptions = {
    format: function(value) {
        return parseFloat(value / 1000).toFixed(2) + ' KB';
    },
    title: 'Network Activity',
    data: ['recv', 'send']
};

class Network extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="container">
        <Chart 
            chartOptions={chartOptions} 
            startDate={this.props.startDate} 
            endDate={this.props.endDate}
        />
        </div>;
    }
}

export default Network;
