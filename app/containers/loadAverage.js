import React from 'react';
import Chart from '../components/chart';

const chartOptions = {
    format: function(value) {
        return parseInt(value * 100) + "%";
    },
    title: 'Load Average',
    data: ['1m', '5m', '15m']
};

class LoadAverage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="container">
        <Chart 
            chartOptions={chartOptions} 
            startDate={this.props.startDate} 
            endDate={this.props.endDate}
        />
        </div>;
    }
}

export default LoadAverage;
