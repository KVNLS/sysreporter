import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import SinglePage from './containers/singlePage';

function AppRouter() {
  return (
    <Router>
        <Route path="/" exact component={SinglePage} />
        <Route path="/network" component={SinglePage} />
        <Route path="/cpu" component={SinglePage} />
        <Route path="/memory" component={SinglePage} />
        <Route path="/loadaverage" component={SinglePage} />
        <Route path="/disk" component={SinglePage} />
        <Route path="/dashboard" component={SinglePage} />
    </Router>
  );
}

export default AppRouter;